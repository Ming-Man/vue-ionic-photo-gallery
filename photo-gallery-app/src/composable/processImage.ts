import * as faceapi from 'face-api.js';
import {Photo} from '@/composable/usePhotoGallery';
import { ref } from 'vue';

export function processImage() {
    let modelsLoaded = false;

    const isProcessingImage = ref(false)
    const loadModels = async () => {
        console.info('LOADING MODELS')
        try {
            console.info('try load loadSsdMobilenetv1Model')
            await faceapi.loadSsdMobilenetv1Model('/models')

            console.info('try load loadFaceLandmarkModel')
            await faceapi.loadFaceLandmarkModel('/models')

            console.info('try load loadFaceExpressionModel')
            await faceapi.loadFaceExpressionModel('/models')

            console.info('modelsLoaded is true')
            modelsLoaded = true

        } catch(e) {
            console.error('errr', e)
        }


    }
    const drawOverlay = async (photo: Photo, input: HTMLImageElement, detections: faceapi.WithFaceLandmarks<any>) => {

        const displaySize = { width: input.width, height: input.height }
        // resize the overlay canvas to the input dimensions
        const canvas = document.getElementById(`canvas-${photo.filepath}`) as HTMLCanvasElement
        faceapi.matchDimensions(canvas, displaySize)

        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        // draw detections into the canvas
        faceapi.draw.drawDetections(canvas, resizedDetections)
        // draw the landmarks into the canvas
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)

        const minConfidence = 0.05
        faceapi.draw.drawFaceExpressions(canvas, resizedDetections, minConfidence)
    }

    const detectFace = async (photo: Photo) => {
        isProcessingImage.value = true
        if (!modelsLoaded) {
            await loadModels()
        }


        console.log('after loading modesl')
        const input = document.getElementById(`img-${photo.filepath}`) as HTMLImageElement
        const detections = await faceapi.detectAllFaces(input as faceapi.TNetInput).withFaceLandmarks().withFaceExpressions()
        console.log({detections, input, photo})
        await drawOverlay(photo, input, detections)
        isProcessingImage.value = false
    }

    return {
        detectFace,
        isProcessingImage
    }
}
