# README #



### What is this repository for? ###

Getting to know Ionic vue

### How do I get set up? ###

- Go to your new project: cd ``.\photo-gallery-app``
- Run ``npm install``
- Run ``ionic serve`` within the app directory to see your app in the browser
- Run ``ionic capacitor add`` to add a native iOS or Android project using Capacitor (already done for android)
- Run ``ionic cap run android -l --external`` to run android app 
- Explore the Ionic docs for components, tutorials, and more: https://ion.link/docs
- Building an enterprise app? Ionic has Enterprise Support and Features: https://ion.link/enterprise-edition


### Next steps Tab2: ###
- [x] apply face recognition on photos
- [x] determine emotions based on picture
- [x] draw data on image

### Next steps Tab3: ###
- [ ] apply OCR from picture

### Next steps Tab4: ###
- [ ] explore web AR [see](https://developers.google.com/web/updates/2018/06/ar-for-the-web)
